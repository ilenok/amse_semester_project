#ifndef UNIT_TEST_H
#define UNIT_TEST_H



class  UnitTest
{
	public:
	UnitTest();	
	void error(bool ex, const char * fname, int line, const char * exps, const char * func);
	virtual void run(int number_of_tests) = 0;
	void report();	
	
	//private:
	int numOK;
	int numFail;
};

#define CHECK(exp)	error(exp,  __FILE__  , __LINE__ , #exp, __func__);

#endif
