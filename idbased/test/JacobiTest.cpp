#include"../src/big_int.h"
#include"JacobiTest.h"
#include"../src/residue_math.h"
#include<cstdlib>
#include<iostream>
#include<fstream>

JacobiTest::JacobiTest() {}

void JacobiTest::test_square()
{
	ZZ mod = RandomLen_ZZ(10);
	if (mod % 2 == 0) mod ++;
	ZZ a = RandomLen_ZZ(7);
	ZZ s = a * a;
	s %= mod;
	int j =  calc_Jacobi(s, mod);
	if (gcd(a, mod)==1)
	{
		CHECK(j == 1);
	}	
	
}
void JacobiTest::test_not_square()
{
	
}
void JacobiTest::run(int number_of_tests)
{
	for (size_t i = 0; i < number_of_tests; ++i) test_square();	
	test_square();
}


