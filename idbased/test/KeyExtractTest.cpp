#include"../src/big_int.h"
#include"KeyExtractTest.h"

#include<cstdlib>
#include<iostream>

KeyExtractTest::KeyExtractTest() {}

void KeyExtractTest::test_square()
{
	
	big_int r0 = RandomLen_ZZ(7);
	big_int a = r0 * r0;
	big_int r = pkg.keyextract(a);
	CHECK( r*r % pkg.get_mpk() == a);
	
}

void KeyExtractTest::test_not_square()
{
	
	big_int r0 = RandomLen_ZZ(7);
	big_int a = r0 * r0;
	a %= pkg.get_mpk();
	a = pkg.get_mpk() - a;
	big_int r = pkg.keyextract(a);
	CHECK( r*r % pkg.get_mpk() == pkg.get_mpk() - a);
	
}

void KeyExtractTest::run(int number_of_tests)
{
	
	pkg.setup("test_msk.txt", "test_mpk.txt");
	for (size_t i  = 0; i  !=  number_of_tests; ++i)
	{
		test_square();
		test_not_square();
	
	}
}


