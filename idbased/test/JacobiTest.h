#ifndef JACOBI_TEST_H
#define JACOBI_TEST_H

#include"UnitTest.h"

struct JacobiTest:UnitTest
{
	JacobiTest();
	void test_square();
	void test_not_square();
	void run(int number_of_tests);
	

};

#endif
