#include<iostream>
#include<fstream>
#include "UnitTest.h"
	

UnitTest::UnitTest()
{
	numOK = 0;
	numFail = 0;
}

void UnitTest::error(bool ex, const char * fname, int line, const char * exps, const char * func)
{
	if (ex)  
	{
		numOK ++;
	}
	else
	{
		numFail ++;
		std:: cout << "Test failed : in " << fname << " in  line " << line << " in function " << func << " " << exps<< "\n"; 
	}
}



void UnitTest::report()
{	
	std::cout <<"OK: "<< numOK << " \n";
	std:: cout <<"Fail: " << numFail << "\n";
	
}


