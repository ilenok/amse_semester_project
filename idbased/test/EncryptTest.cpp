#include"../src/big_int.h"
#include"EncryptTest.h"
#include"../src/residue_math.h"

#include<cstdlib>
#include<iostream>

EncryptTest::EncryptTest() {}

void EncryptTest::test_bit()
{
	int x = rand() % 2;
	bool correct;
	Pair p = cl.encrypt_bit(x, correct);
	int y = cl.decrypt_bit(p);
	//if (correct)
		CHECK(x == y);
	
}

void EncryptTest::run(int number_of_tests)
{	
	cl.load_mpk("mpk.txt");
	cl.load_pk("pk.txt");
	cl.load_sk("sk.txt");					
	for (size_t i = 0; i < number_of_tests; ++i) test_bit();
	
}


