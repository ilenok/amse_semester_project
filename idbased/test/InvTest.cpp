#include"../src/big_int.h"
#include"InvTest.h"
#include"../src/residue_math.h"
#include<cstdlib>
#include<iostream>

InvTest::InvTest() {}

void InvTest::test()
{
	big_int mod = RandomLen_ZZ(10);
	big_int a = RandomLen_ZZ(9);
	big_int b;
        if (inv(a, mod, b))
	{
		CHECK((a * b) % mod == 1) ;
	}
}

void InvTest::run(int number_of_tests)
{
	for (size_t i = 0; i < number_of_tests ; ++i) test();
	
}


