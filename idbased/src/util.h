#ifndef UTIL_H
#define UTIL_H

#include"big_int.h"
#include<map>

ZZ hash(const char * identity, ZZ & mpk);
int get(unsigned char val, int pos);
const char**  split_string(std::string s,  int & n);


#endif
