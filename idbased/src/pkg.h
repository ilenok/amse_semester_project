#ifndef PKG_H
#define PKG_H

#include"big_int.h"

class Pkg
{
	public:
	Pkg();
	void setup(const char * msk_file_name, const char * mpk_file_name, int security = 15);
	void setup(string msk_file_name, string mpk_file_name, int security = 15);

	void set_msk(ZZ & p, ZZ  & q);
	void calc_mpk();

	void save_msk(const char * msk_file_name);
	void save_mpk(const char * mpk_file_name);	

	Pair get_msk();
	ZZ get_mpk();	
	
	void load_msk(const char * msk_file_name);	
	void load_msk(string msk_file_name);

	ZZ keyextract(const ZZ & pk);
	void keyextract_from_string(string identity, string sk_file_name);
	//void keyextract(const char * pk_file_name, const char * sk_file_name);
	void keyextract(string pk_file_name, string sk_file_name);
	void keyextract(string pk_file_name, string  sk_file_name, string password);
	void encrypt_sk(FILE * sk_file, string password, ZZ & sk);


	private:
	ZZ mpk; //public key
	ZZ msk_1; //secret keys
	ZZ msk_2;
};
#endif
