#include"pkg.h"
#include"client.h"
#include"util.h"
#include"help.h"
#include"sha1.h"
#include"my_exceptions.h"
#include"CmdParser.h"

#include<fstream>
#include<iostream>
#include <cstdlib>
#include<cstring>
#include<string>
#include<map>

using std::string;

int main(int argc,const  char ** argv)
{
	Pkg keygen;
	Client cl;
	CmdParser cmdp(argc, argv);
	map<string, string> &  file_names = cmdp.file_names;

	try
	{
		cmdp.parse_arguments();
	}
	catch (WrongParameterException e)

	{
		std:: cout << "Error:\nNo parameter " << e.what() << " for this command\n";
		return 0;
	}
	catch (MissingArgumentException e)

	{
		std:: cout << "Error:\nMissing argument after " << e.what() << "\n";
		return 0;
	}



	string warning = cmdp. create_default_filenames_warning();


	if (!strcmp(cmdp.first_arg, "--help"))
	{
		std:: cout << help;
		return 0;
	}
	if (!strcmp(cmdp.first_arg, "setup"))
	{

		keygen.setup(file_names["-msk"], file_names["-mpk"]);
		std:: cout << "master public key:" << keygen.get_mpk()<< " \n";
		std:: cout << warning;
		return 0;
	}
	if (!strcmp(cmdp.first_arg, "keyextract"))
	{
		try
		{
			keygen.load_msk(file_names["-msk"]);
			keygen.calc_mpk();
			keygen.keyextract(file_names["-pk"], file_names["-sk"]);
			std:: cout << warning;
		}
		catch (FileNotFoundException e)
		{
			std:: cout << "Error:\nFile "<< e.what() << " not found \n";
			return 0;
		}
	}
	if (!strcmp(cmdp.first_arg, "encrypt"))
	{
		try
		{
			cl.load_mpk(file_names["-mpk"]);
			cl.load_pk(file_names["-pk"]);
			cl.encrypt(file_names["-m"],  file_names["-c"]);
			std:: cout << warning;
		}
		catch (FileNotFoundException e)
		{
			std:: cout <<"Error:\nFile "<< e.what() << " not found \n";
			return 0;
		}
	}
	if (!strcmp(cmdp.first_arg, "decrypt"))
	{
		try
		{
			cl.load_mpk(file_names["-mpk"]);
			cl.load_pk(file_names["-pk"]);
			cl.load_sk(file_names["-sk"]);
			cl.decrypt(file_names["-c"], file_names["-d"]);
			std:: cout << warning;
		}

		catch (FileNotFoundException e)
		{
			std:: cout << "Error:\nFile "<< e.what() << " not found \n";
		}
	}
	if (!strcmp(cmdp.first_arg, "--help"))
	{
		std:: cout << help ;
		return 0;
	}
	if (!strcmp(cmdp.first_arg, "setup_help"))
	{
		std:: cout << setup_help ;
		return 0;
	}
	if (!strcmp(cmdp.first_arg, "keyextract_help"))
	{
		std:: cout << keyextract_help;
		return 0;
	}
	if (!strcmp(cmdp.first_arg, "encrypt_help"))
	{
		std:: cout << encrypt_help ;
		return 0;
	}
	if (!strcmp(cmdp.first_arg, "decrypt_help"))
	{
		std:: cout << decrypt_help ;
		return 0;
	}

	return 0;


}
