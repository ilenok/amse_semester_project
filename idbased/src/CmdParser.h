
#ifndef CMD_PARSER_H
#define CMD_PARSER_H
#include <set>
#include<string>
#include<map>
#include "help.h"


using std:: string;
using std::map;
using std::set;

struct  CmdParser
{	
	CmdParser(int i,  const char ** str);	
	void parse_arguments();
	void set_default_file_names();
	string create_default_filenames_warning();

	//private:
	int argc;
	const char ** argv;
	const char * first_arg;
	map<string, std:: string> file_names;
	map<string, bool>  file_omitted;
	std::set <string>  keys;
};

#endif
