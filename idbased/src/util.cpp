#include"util.h"
#include"sha1.h"
#include"residue_math.h"

#include<cstring>
#include<sstream>

#include<iostream>

ZZ hash(const char * identity, ZZ & mpk)
{
	SHA1 sha;
	sha.Reset();
			
	unsigned int out[5];
	sha << identity;
	sha.Result(out);
	
	
	unsigned char out_to_char[sizeof(int)*5];
	memcpy(out_to_char,  out , sizeof(int)*5);
	ZZ a = ZZFromBytes(out_to_char,  sizeof(int)*5);//to_ZZ(out[4]);
	a %= mpk;
	while (calc_Jacobi(a, mpk) != 1) a++;
	a %= mpk;
	return a;
}

int get(unsigned char val, int pos)
{
	unsigned char mask = 128;
	for (size_t i = 0; i != pos; ++i)
	{
		mask /= 2;
	}
	return (bool) (mask & val);
}

const char** split_string(std::string s,  int & n)
{
	const char ** ww = new const char * [256];
	istringstream iss(s);
	const char ** w = ww;
	std::string sub;
	while (iss >>  sub) 
	{
		*w= sub.c_str();		
		w++;
	}
	
	n =  w - ww ;
	return ww;
}
	

