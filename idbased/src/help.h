#ifndef HELP_H
#define HELP_H

#include <string>
#include<map>
using std::string;

const std:: string encrypt_help = "encrypt\n [-m	message_file] \n [-c	ciphertext_file]\n [-mpk 	master_public_key_file]\n [-pk	public_key_fil]e \nEncrypts message_file using keys in two last files. Writes in ciphertext_file\n";
const std:: string decrypt_help ="decrypt\n [-c	ciphertext_file] \n [-d	decrypted_message_file]\n [-mpk	master_public_key_file]\n [-pk	public_key_file]\n [-sk	secret_key_file]\nDecrypts ciphertext_file using keys in three last files. Writes in decrypted_message_file\n";
const std:: string keyextract_help = "keyextract\n [-mpk	master_secret_key_file] \n [-pk	public_key_file] \n [-sk	secret_key_file]\nUsing master secret key and public key calculates secret key and writes it in secret_key_file\n ";
const std:: string setup_help = "setup\n [-msk	master_secret_key_file]\n [-mpk	master_public_key_file] \nGenerates new master secret key and master public key\n ";

const std::string default_msk_warning = "By default, master secret key file is \"msk.txt\"\n";
const std::string default_mpk_warning = "By default, master public key file is \"mpk.txt\"\n";
const std::string default_sk_warning = "By default, secret key file is \"sk.txt\"\n";
const std::string default_pk_warning = "By default, public key file is \"pk.txt\"\n";
const std::string default_pks_warning = "";
const std::string default_message_warning = "By default, message file is \"m.txt\"\n";
const std::string default_ciphertext_warning = "By default, ciphertext file is \"c.txt\"\n";
const std::string default_decrypted_message_warning = "By default, decrypted message file is \"d.txt\"\n";

const std::string help = "Syntax:\n ./bin/program	command	keys\nCommands:\nsetup:	generates new master public key \nkeyextract: generates secret key \nencrypt: encrypts message\ndecrypt: decrypts message\nFor more information, type\n ./bin/program	command	 --help\n";
#endif
