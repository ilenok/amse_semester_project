#ifndef RESMATH_H
#define RESMATH_H

#include"big_int.h"

ZZ pow(const ZZ & base, long p);
ZZ pow(const ZZ & base, const ZZ & p,const ZZ & mod);

ZZ gen_prime(long l);
ZZ gen_strong_prime(long l);

ZZ rand_mod(const ZZ & mod); //random integer from  0 to mod-1


ZZ gcd(const ZZ & a, const  ZZ & b);
bool inv(const ZZ & n, const ZZ & mod, ZZ &res); //find inverse

int calc_Jacobi(const ZZ & a, const ZZ & n); //calculates the Jacobi symbol
ZZ find_Jacobi_eq(int t, const ZZ & mod); //finds an integer a so that the
// Jacobi symbol for  a and mod equals t

#endif
