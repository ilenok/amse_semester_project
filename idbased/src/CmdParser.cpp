#include"CmdParser.h"
#include"help.h"
#include"my_exceptions.h"

#include<cstring>
#include <iostream>
CmdParser::CmdParser(int i,  const char ** str)
{
		argc = i;
		argv = str;
		
}

void CmdParser::parse_arguments()
{
	if (argc == 1)
	{
		first_arg = "--help";					
	}
	std::set<string>  help_keys ;
	help_keys .insert("--help");
	help_keys .insert("setup_help");
	help_keys .insert("keyextract_help");
	help_keys .insert("encrypt_help");
	help_keys .insert("decrypt_help");
	if (argc >1)
	{
		first_arg = argv[1];
		keys.insert("--help");
		if (!strcmp(first_arg, "setup"))
		{
			if ( (argc>2)&& (!strcmp(argv[2], "--help")))
			{
				first_arg = "setup_help";		
			}
			else
			{
				keys.insert("-mpk");
				keys.insert("-msk");
			}
		}
		else if (!strcmp(first_arg, "keyextract"))	
		{
			if ((argc>2) && (!strcmp(argv[2], "--help")))
			{
				first_arg = "keyextract_help";		
			}
			else
			{
				keys.insert("-msk");
				keys.insert("-pk");
				keys.insert("-sk");
			}
		}
		else if (!strcmp(first_arg, "encrypt"))	
		{
			if ((argc>2)&& (!strcmp(argv[2], "--help")))
			{
				first_arg = "encrypt_help";		
			}
			else
			{
				keys.insert("-mpk");
				keys.insert("-pk");
				keys.insert("-m");
				keys.insert("-c");
			}
			
		}
		else if (!strcmp(first_arg, "decrypt"))	
		{
			if ((argc>2)&& (!strcmp(argv[2], "--help")))
			{
				first_arg = "decrypt_help";		
			}
			else
			{
				keys.insert("-mpk");
				keys.insert("-pk");
				keys.insert("-sk");
				keys.insert("-d");
				keys.insert("-c");
			}
		}
		else  first_arg = "--help";
		if (help_keys.find(first_arg) == help_keys.end())
		{			
			set_default_file_names();
			for (set<string>:: iterator it= keys.begin(); it != keys.end(); ++it)
			{
				file_omitted[*it] = true;
			}
			set<string>:: iterator iter;
			size_t i =2;
			while (i != argc)
			{
				string s = argv[i];
				iter = keys.find(s);
				if (iter != keys.end())
				{
					++i;
					if (i != argc)
					{
						string s_2 = argv[i];
						iter = keys.find(s_2);
						if (iter != keys.end()) 
						{
							throw MissingArgumentException(s);
						}
						else
						{
							file_names[s] = s_2;
							file_omitted[s] = false;
							++i;
						}
					}
					else throw MissingArgumentException(s);
				}				
				else throw WrongParameterException(s);			
			}
		}	
	}
}

string CmdParser::create_default_filenames_warning()
{
	string s ="";
	map <string, string> warning;
	warning["-msk"] = default_msk_warning ;
	warning["-mpk"] = default_mpk_warning ;
	warning["-pk"] = default_pk_warning ;	
	warning["-sk"] = default_sk_warning ;
	warning["-m"] = default_message_warning ;
	warning["-c"] = default_ciphertext_warning ;
	warning["-d"] =default_decrypted_message_warning ;
	for (set<string>:: iterator it= keys.begin(); it != keys.end(); ++it)
	{
		if (file_omitted[*it] ==  true)
		{
			s += warning[*it];
		}
	}
	return s;
}

void CmdParser:: set_default_file_names()
{
	for (set<string>:: iterator it = keys.begin(); it != keys.end(); ++it)
	{
		string key= *it;
		string fn=key;
		fn.erase(0,1);
		fn += ".txt";
		file_names[key] = fn;
	}
}
