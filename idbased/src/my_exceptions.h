#ifndef MYEXCH
#define MYEXC_H
#include  <string>

class MissingArgumentException: public std::exception
{
	private:
	std::string key;	

	public:
	 MissingArgumentException(const std::string & s) throw() : key(s) {}
        ~ MissingArgumentException() throw() {}
        virtual const char* what() const throw() { return key.c_str(); }

};

class WrongParameterException: public std::exception
{
	private:
	std::string key;	

	public:
	WrongParameterException(const std::string & s) throw() : key(s) {}
        ~ WrongParameterException() throw() {}
        virtual const char* what() const throw() { return key.c_str(); }

};

class FileNotFoundException: public std::exception
{
	private:
        std::string  file_name;

	public:
        FileNotFoundException(const std::string & s) throw() : file_name(s) {}
        ~FileNotFoundException() throw() {}
        virtual const char* what() const throw() { return file_name.c_str(); }
};




#endif
