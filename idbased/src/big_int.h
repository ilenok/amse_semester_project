#ifndef BIGINT_H
#define BIGINT_H

#include<utility>
#include <NTL/ZZ.h>

NTL_CLIENT

typedef ZZ big_int;
typedef std::pair<big_int, big_int> Pair;

static const size_t KEY_LENGTH = 16;
static const size_t BLOCK_LENGTH = 16;
static const size_t NUMBER_OF_BITS= 8;
static const size_t HASH_LENGTH = 5;
using std::string;

#endif
