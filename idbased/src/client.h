#ifndef CLIENT_H
#define CLIENT_H

#include"big_int.h"

class Client
{
	public: 

	void set_mpk(const ZZ & n);
	void set_pk(const ZZ  & n);
	void set_sk(const ZZ & n);

	ZZ get_pk();
	ZZ get_sk();
	ZZ get_mpk();
	
	void load_mpk(const string & mpk_file_name );	
	void load_pk(const string & pk_file_name );
	void load_sk(const string & sk_file_name );	
	void load_sk( const string & sk_file_name, string password);
	void decrypt_sk(FILE * sk_file, string password);
	
	Pair encrypt_bit(int x);
	Pair encrypt_bit(int x, bool & correct);
	//Before calculating  Jacobi symbol checks if numbers are relatively prime

	
	int decrypt_bit(const Pair & p);	

	void encrypt(const string & message_file_name, const string & ciphertext_file_name );
	void decrypt(const string & ciphertext_file_name, const string & decrypted_message_file_name);


	private:
	unsigned char * gen_aes_key();	
	
	ZZ pk; //public key
	ZZ sk; //secret key
	ZZ mpk; //master public key

	static const size_t NUMBER_OF_BITS= 8;
	static const size_t HASH_LENGTH = 5;
	static const size_t KEY_LENGTH = 16;
	static const size_t BLOCK_LENGTH = 16;
};
#endif
